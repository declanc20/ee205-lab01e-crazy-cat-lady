///////////////////////////////////////////////////////////////////////////////
//           University of Hawaii, College of Engineering
/// @brief   Lab01e - Crazy Cat Lady - EE 205 - Spr 2022
///
/// @file    crazyCatLady.cpp
/// @version 1.0 - Initial version
///
/// Compile: $ g++ -o crazyCatLady crazyCatLady.cpp
///
/// Usage:  crazyCatLady catName
///
/// Result:
///   Oooooh! [catName] you’re so cute!
///
/// Example:
///   $ ./crazyCatLady Snuggles
///   Oooooh! Snuggles you’re so cute!
///
/// @author   declan campbell < declanc@hawaii.edu>
/// @date    12_01_2022
///////////////////////////////////////////////////////////////////////////////


#include <iostream>

int main( int argc, char* argv[] ) {
	//std::cout << "Crazy Cat Lady!" << std::endl ; this line demonstrated to us how to print to screen

   char* catname = argv[1]; //the catname is a string which we pull from the command line 
   
	std::cout << "Oooooh! "<< catname << " you’re so cute!" << std::endl; //prints out the desired name with remark

   return 0;
}
