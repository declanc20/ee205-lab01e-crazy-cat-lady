###############################################################################
###          University of Hawaii, College of Engineering
### @brief   Lab 01e - Crazy Cat lady - EE 205 - Spr 2022
###
### @file    Makefile
### @version 1.0 - Initial version
###
### 
###
### @author   Declan Campbell < declanc@hawaii.edu>
### @date     12_1_2002
###
### @see     https://www.gnu.org/software/make/manual/make.html
###############################################################################

CPP     = g++
CFLAGS = -g -Wall

TARGET = crazyCatLady

all: $(TARGET)

crazyCatGuy: crazyCatLady.cpp
	$(CPP) $(CFLAGS) -o $(TARGET) crazyCatLady.cpp

clean:
	rm -f $(TARGET) *.o

